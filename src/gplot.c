#include <stdio.h>
#include <string.h>

#include "gplot.h"

int update_plot_options(const struct plotindex pi, nve dxv)
{

  if ( pi.nx == -1 )
    set_str("x","T");
  else if ( pi.nx < dxv.nbr_el && pi.nx >= 0 )
    set_str("x",dxv.name[pi.nx]);
  if ( pi.ny == -1 )
    set_str("y","T");
  else if ( pi.ny < dxv.nbr_el && pi.ny >= 0 )
    set_str("y",dxv.name[pi.ny]);
  if ( pi.nz == -1 )
    set_str("z","T");
  else if ( pi.nz < dxv.nbr_el && pi.nz >= 0 )
    set_str("z",dxv.name[pi.nz]);

  set_int("x",pi.nx);
  set_int("y",pi.ny);
  set_int("z",pi.nz);

  return 1;

}

int update_plot_index(struct plotindex *pi, nve dxv)
{
  char sval[NAMELENGTH];
  strlcpy(sval,get_str("x"),NAMELENGTH);
  if ( strlen(sval) )
  {
    name2index(sval,dxv,&(pi->nx));
  }
  strlcpy(sval,get_str("y"),NAMELENGTH);
  if ( strlen(sval) )
  {
    name2index(sval,dxv,&(pi->ny));
  }
  strlcpy(sval,get_str("z"),NAMELENGTH);
  if ( strlen(sval) )
  {
    name2index(sval,dxv,&(pi->nz));
  }
  set_int("x",pi->nx);
  set_int("y",pi->ny);
  set_int("z",pi->nz);
  pi->x = pi->nx+2;
  pi->y = pi->ny+2;
  pi->z = pi->nz+2;

  return 1;

}


int gplot_normal(const struct plotindex pi, const int plot3d, const nve dxv)
{
  char  plot_cmd[CMDLENGTH];
  char  key[EXPRLENGTH];
  int   index_mfd = 1 + SIM->nbr_var + SIM->nbr_aux + SIM->nbr_psi;
  char  filename[NAMELENGTH];
  char  fileformat[NAMELENGTH];
  char  mode[16];
  int   cx = pi.x, cy = pi.y, cz = pi.z;

  /* generate the data to plot: if pi.x, pi.y or pi.z are MF then use STATS_FILENAME file
   * otherwise use particle file idXX */
  if ( ( pi.x <= index_mfd || pi.x > (index_mfd + SIM->nbr_mfd) ) &&
       ( pi.y <= index_mfd || pi.y > (index_mfd + SIM->nbr_mfd) ) &&
       ( pi.z <= index_mfd || pi.z > (index_mfd + SIM->nbr_mfd) || plot3d == 0 ) )
  {
    snprintf(filename,NAMELENGTH, ODEXPDIR "id%d.dat",get_int("particle"));
    snprintf(fileformat,NAMELENGTH,"%%%dlf",SIM->nbr_col);
    snprintf(mode,16,"%d",get_int("particle"));
  }
  else
  {
    snprintf(filename,NAMELENGTH,STATS_FILENAME);
    snprintf(fileformat,NAMELENGTH,"%%%dlf%%5d",1 + SIM->nbr_mfd);
    snprintf(mode,16,"%s","pop avg");
    if ( pi.x > index_mfd && pi.x <= (index_mfd + SIM->nbr_mfd) ) cx -= index_mfd - 1;
    if ( pi.y > index_mfd && pi.y <= (index_mfd + SIM->nbr_mfd) ) cy -= index_mfd - 1;
    if ( pi.z > index_mfd && pi.z <= (index_mfd + SIM->nbr_mfd) ) cz -= index_mfd - 1;
  }

  /* set plot key (legend) to plotkey if non-empty */
  if ( strlen(get_str("plotkey")) )
  {
    snprintf(key,EXPRLENGTH,"\"%s\"", get_str("plotkey"));  
  }
  else if ( plot3d == 0 )
  {
    snprintf(key,EXPRLENGTH, "\"%s\".\" vs \".\"%s\". \" \" .\"(%s)\"",
        pi.y > 1 ? dxv.name[pi.y-2] : get_str("indvar"), \
        pi.x > 1 ? dxv.name[pi.x-2] : get_str("indvar"),
        mode);
  }
  else
  {
    snprintf(key,EXPRLENGTH,"(%d)", get_int("particle")); 
  }

  /* make plot command */
  if ( plot3d == 0 )
  {
    snprintf(plot_cmd,CMDLENGTH,\
        "\"%s\" binary format=\"%s\" using %d:%d "\
        "with %s title %s\n",\
        filename, fileformat, cx, cy,\
        get_str("style"),  key);
    if ( get_int("hold") == 0 ) /* normal plot command: 2D, hold=0, curves=0 */
    {
      fprintf(GPLOTP,"plot %s", plot_cmd);
    }
    else if ( get_int("hold") )
    {
      fprintf(GPLOTP,"replot %s", plot_cmd);
    }
  } 
  else /* plot3d == 1 */
  {
    snprintf(plot_cmd,CMDLENGTH,\
        "\"%s\" binary format=\"%s\" using %d:%d:%d "\
        "with %s title \"%s\"\n",\
        filename, fileformat, cx, cy, cz,\
        get_str("style"),key);
    if ( get_int("hold") == 0 )
    {
      fprintf(GPLOTP,"splot %s", plot_cmd);
    }
    else
    {
      fprintf(GPLOTP,"replot %s", plot_cmd);
    }
  }
  fflush(GPLOTP);
  return 0;
}

int gplot_animate(const struct plotindex pi, const int plot3d, const nve dxv, const char *optstring)
{
  char    plot_cmd[CMDLENGTH];
  const int nbr_col = SIM->nbr_col; 
  set_int("hold",0); /* animate only works without hold at the moment */
  if ( plot3d == 0 )
  {
    generate_particle_file(get_int("particle"));
    snprintf(plot_cmd,CMDLENGTH,\
        "do for [j=0:%d] {"
        "plot \"" ODEXPDIR "id%d.dat\" binary format=\"%%%dlf\" using %d:%d every ::0::j "
        "with %s notitle, "
        "\"" ODEXPDIR "id%d.dat\" binary format=\"%%%dlf\" using %d:%d every ::j::j "
        "with %s title \"%s\".\" vs \".\"%s\". \" \" .\"(%d)\" %s\n"
        "}\n",
        get_int("res") - 1,
        get_int("particle"), nbr_col, pi.x, pi.y,
        get_str("style"), get_int("particle"), nbr_col, pi.x, pi.y,
        get_str("particlestyle"), pi.y > 1 ? dxv.name[pi.y-2] : get_str("indvar"), pi.x > 1 ? dxv.name[pi.x-2] : get_str("indvar"), get_int("particle"), optstring);
    fprintf(GPLOTP,"%s", plot_cmd);
  } 
  else /* plot3d == 1 */
  {
    generate_particle_file(get_int("particle"));
    snprintf(plot_cmd,CMDLENGTH,\
        "do for [j=0:%d] {"
        "splot \"" ODEXPDIR "id%d.dat\" binary format=\"%%%dlf\" using %d:%d:%d every ::0::j "
        "with dots notitle, "
        "\"" ODEXPDIR "id%d.dat\" binary format=\"%%%dlf\" using %d:%d:%d every ::j::j "
        "with %s title \"%s\".\", \".\"%s\".\", \".\"%s\". \" \" .\"(%d) %s\"}\n",
        get_int("res") - 1,
        get_int("particle"), nbr_col, pi.x, pi.y, pi.z,
        get_int("particle"), nbr_col, pi.x, pi.y, pi.z,
        get_str("particlestyle"), pi.x > 1 ? dxv.name[pi.x-2] : get_str("indvar"), pi.y > 1 ? dxv.name[pi.y-2] : get_str("indvar"), pi.z > 1 ? dxv.name[pi.z-2] : get_str("indvar"), get_int("particle"), optstring);
    fprintf(GPLOTP,"%s", plot_cmd);
  }
  fflush(GPLOTP);
  return 0;
}

/* plot data from file data_fn */
int gplot_data(const char *plot_str, const char *data_fn)
{
  char key[EXPRLENGTH];
  if ( strlen(get_str("plotkey")) )
  {
    strlcpy(key,get_str("plotkey"),EXPRLENGTH);
  }
  else
  {
    strlcpy(key,get_str("data2plot"),EXPRLENGTH);
  }
  if ( get_int("hold") || get_int("curves") )
  {
    fprintf(GPLOTP,\
      "replot \"%s\" u %s w %s title \"%s\"\n",\
      data_fn,plot_str,get_str("datastyle"), key);
  }
  else
  {
    fprintf(GPLOTP,\
      "plot \"%s\" u %s w %s title \"%s\"\n",\
      data_fn,plot_str,get_str("datastyle"), key);
  }
  fflush(GPLOTP);
  return 0;
}

int gplot_fun(const char *plot_str)
{
  const int nbr_col = SIM->nbr_col; 
  char key[EXPRLENGTH];
  generate_particle_file(get_int("particle"));
  if ( strlen(get_str("plotkey")) )
  {
    strlcpy(key,get_str("plotkey"),EXPRLENGTH);
  }
  else
  {
    strlcpy(key,plot_str,EXPRLENGTH);
  }
  if ( get_int("hold") )
  {
    fprintf(GPLOTP,\
          "replot \"" ODEXPDIR "id%d.dat\" binary format=\"%%%dlf\" using %s "\
          "with %s title \"%s (%d)\"\n",\
          get_int("particle"), nbr_col, plot_str,\
          get_str("style"), key, get_int("particle"));
  }
  else
  {
    fprintf(GPLOTP,\
          "plot \"" ODEXPDIR "id%d.dat\" binary format=\"%%%dlf\" using %s "\
          "with %s title \"%s (%d)\"\n",\
          get_int("particle"), nbr_col, plot_str,\
          get_str("style"), key, get_int("particle"));
  }
  fflush(GPLOTP);
  return 0;
}

int gplot_particles( const struct plotindex pi, const nve var)
{
  char cmd_plt[2*CMDLENGTH];
  char cmd_labels[CMDLENGTH];
  char opt_circle[EXPRLENGTH];

  /* variables that can plotted
   * type   length    
   * id     1         
   * y      nbr_var   
   * aux    nbr_aux   
   * psi    nbr_psi   
   * mfd    nbr_mfd   
   * pex    nbr_expr  
   */
  const int nvar = SIM->nbr_col-1;  

  /* if x-axis is the independent variable,
   * make it ID number instead--the ind var
   * cannot be plotted 
   */
  fprintf(GPLOTP,"set xlabel '%s'\n",pi.x > 1 ? var.name[pi.x-2] : "ID"); 

  fprintf(GPLOTP,"set ylabel '%s'\n",var.name[pi.y-2]);


  if ( strncmp("none",get_str("particleweight"),4) )
  {
    snprintf(opt_circle,EXPRLENGTH,":(%s)",get_str("particleweight"));
  }
  else
  {
    /* snprintf(opt_circle,1,""); */
    opt_circle[0] = 0;
  }

  /* option: plot bivariate kernel density estimate */
  if ( get_int("kdensity2d") )
  {
    fprintf(GPLOTP,"set dgrid3d %d,%d, gauss kdensity %g\n",
        get_int("kdensity2dgrid"),get_int("kdensity2dgrid"),get_dou("kdensity2dscale"));
    fprintf(GPLOTP,"set table \"" ODEXPDIR "griddata.txt\"\n");
    fprintf(GPLOTP,"splot \"" ODEXPDIR "particle_states.dat\" "
        "binary format=\"%%u%%%dlf\" using %d:%d:(1)\n",nvar,pi.x,pi.y);
    fprintf(GPLOTP,"unset table\n");
    fprintf(GPLOTP,"unset dgrid3d\n");
    fprintf(GPLOTP,"set view map\n");
    fprintf(GPLOTP,"set autoscale fix\n");
    fprintf(GPLOTP,"unset colorbox\n");
    fprintf(GPLOTP,"splot \"" ODEXPDIR "griddata.txt\" with pm3d notitle\n");
    fprintf(GPLOTP,"replot \"" ODEXPDIR "particle_states.dat\" "
        "binary format=\"%%u%%%dlf\" using %d:%d:(-1) with %s notitle\n",
        nvar,pi.x,pi.y,get_str("particlestyle"));
    fprintf(GPLOTP,"unset view\n");
  }
  else
  {
    /* Plot each particle with style particlestyle 
     * Optionally write ID number inside it 
     */
    snprintf(cmd_plt,CMDLENGTH,"plot \"" ODEXPDIR "particle_states.dat\" "
        "binary format=\"%%u%%%dlf\" using %d:%d%s with "
        "%s title \"%s\"",nvar,pi.x,pi.y,opt_circle, get_str("particlestyle"),
        get_str("plotkey"));
    if ( get_int("particleid") )
    {
      snprintf(cmd_labels,CMDLENGTH-1,", \"" ODEXPDIR "particle_states.dat\" "
          "binary format=\"%%u%%%dlf\" using %d:%d:(sprintf(\"%%u\",$1)) "
          "with labels font \"%s,7\" textcolor rgb \"grey20\" notitle\n", 
          nvar,pi.x,pi.y,get_str("font"));
    }
    else
    {
      snprintf(cmd_labels,2,"\n");
    }
    strncat(cmd_plt,cmd_labels,CMDLENGTH);
    fprintf(GPLOTP, "%s", cmd_plt );
  }

  fflush(GPLOTP);

  return 0;
}

int get_plottitle(char *tok) /* set plot key string if 'title "key"' found */
{
  char val[EXPRLENGTH];
  if ( tok != NULL )
  {
    if ( sscanf(tok,"\"%[^\"]\"",val) == 1 )
    {
      set_str("plotkey",val);
      return 0;
    }
  }
  set_str("plotkey","");
  return 1;
}


int setup_pm_normal(const struct plotindex pi, const int plot3d, const nve dxv)
{
  /* set axis labels and plot */
  int total_nbr_x = dxv.nbr_el;
  char svalue[NAMELENGTH];
#if 0
  fprintf(GPLOTP,"unset xrange\n");
  fprintf(GPLOTP,"unset yrange\n");
  fprintf(GPLOTP,"unset zrange\n");
#endif
  if ( get_int("hold") == 0 )
  {
    if (pi.x == 1) /* time evolution: xlabel = 'time' */
    {
      fprintf(GPLOTP,"set xlabel '%s'\n", get_str("indvar"));
    }
    else if ( (pi.x-2) < (int)total_nbr_x ) /* xlabel = name of variable  */
    {
      if ( get_attribute(dxv.attribute[pi.x-2],"tag",svalue) )
      {
        fprintf(GPLOTP,"set xlabel '%s'\n",svalue);
      }
      else
      {
        fprintf(GPLOTP,"set xlabel '%s'\n",dxv.name[pi.x-2]);
      }
    }
    if (pi.y == 1) /* time evolution: ylabel = 'time' */
    {
      fprintf(GPLOTP,"set ylabel '%s'\n", get_str("indvar"));
    }
    else if ( (pi.y-2) < (int)total_nbr_x ) /* variable */
    {
      if ( get_attribute(dxv.attribute[pi.y-2],"tag",svalue) )
      {
        fprintf(GPLOTP,"set ylabel '%s'\n",svalue);
      }
      else
      {
        fprintf(GPLOTP,"set ylabel '%s'\n",dxv.name[pi.y-2]);
      }
    }
    if ( plot3d == 1 )
    {
      if (pi.z == 1) /* time evolution: zlabel = 'time' */
      {
        fprintf(GPLOTP,"set zlabel '%s'\n", get_str("indvar"));
      }
      else if ( (pi.z-2) < (int)total_nbr_x ) /* variable */
      {
        if ( get_attribute(dxv.attribute[pi.z-2],"tag",svalue) )
        {
          fprintf(GPLOTP,"set zlabel '%s'\n",svalue);
        }
        else
        {
          fprintf(GPLOTP,"set zlabel '%s'\n",dxv.name[pi.z-2]);
        }
      }
    }
  }
  else if ( get_int("hold") )  /* hold is on - unset axis labels */
  {
    fprintf(GPLOTP,"set xlabel \"\"\n");
    fprintf(GPLOTP,"set ylabel \"\"\n");
    fprintf(GPLOTP,"set zlabel \"\"\n");
  }
  return 0;
}
