#ifndef _GPLOT_H_
#define _GPLOT_H_

#include "odexp.h"
#include "datastruct.h"

extern FILE* GPLOTP;

struct plotindex { 
 int x, y, z;
 int nx, ny, nz;
};


int update_plot_options(const struct plotindex pi, nve dxv);
int update_plot_index(struct plotindex *pi, nve dxv);
int setup_pm_normal(const struct plotindex pi, const int plot3d, const nve dxv);
int gplot_normal(const struct plotindex pi, const int plot3d, const nve dxv);
int gplot_data(const char *plot_str, const char *datafile_plotted );
int gplot_fun(const char *plot_str);
int gplot_animate(const struct plotindex pi, const int plot3d, const nve dxv, const char *optstring);
int gplot_particles( const struct plotindex pi, const nve var); 

int get_plottitle(char *cmd);

#endif
