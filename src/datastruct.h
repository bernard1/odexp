/* file datastruct.h */

#ifndef _DATASTRUCT_H_
#define _DATASTRUCT_H_



/* includes */
#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "macros.h"
#include "odexp.h"
#include "options.h"

/* color palettes */
extern const char PALETTE_ACID[8][7];
extern const char PALETTE_QUAL[9][7];
extern const char PALETTE_APPLE[8][7];
extern const char PALETTE_MONO[1][7];

typedef struct double_array
{
    double *array;
    int length;
} double_array;

typedef struct steady_state
{
    double *s;
    double *re;
    double *im; 
    int index;
    int size;
    int status;
} steady_state;

int     set_dou(const char *name, const double val); 
int     set_int(const char *name, const int val); 
int     set_str(const char *name, const char * val); 
double  get_dou(const char *name);
int     get_int(const char *name);
char*   get_str(const char *name);

int     name2index( const char *name, nve var, int *n);
int     option_name2index( const char *name, int *n);

void    alloc_namevalexp( nve *mu );
void    free_namevalexp( nve mu );

/* init population of particle_state */
void    init_dlist(dlist *list );

/* init world */
void    init_world( world *s, nve *pex, nve *func, nve *mu,\
          nve *ics, nve *fcn, nve *eqn, nve *psi, nve *mfd,\
          nve *dxv, nve *cst, nve *dfl, int (*ode_rhs)(double, const double *, double *, void *));

/* insert at the end of the list */
int     insert_endoflist( dlist *list, par *pars );
int     par_birth ( void );
int     par_repli (par *pars);

/* delete element pos from the list */
int     delete_el( dlist *list, par *to_del);

/* destroy the list */
void    destroy(dlist *list);

/* destroy the world */
void    free_world(world *s);

void    printf_particle(par *p);

double  getv(char *name, par *p);

par *   getpar( int with_id );

int     fwrite_final_particle_state( void ); 
int     fwrite_all_particles(const double *restrict t);
int     fwrite_SIM(const double *restrict t);
int     list_particle( int with_id );
int     list_stats( void );
int     list_traj( void );

int     generate_particle_file(int with_id);
int     generate_particle_states();

/* get the value of variable s, for particle p, into ptr */ 
/* int     mvar(const char *name, par *m, double *ptr); */

/* remove chars before first occurence of c and after last oocurence of c */
void    trim(char *str, const char c);

/* count number of printed characters in multi-byte char string */
int mbstrlen(const char *string);

int get_attribute(const char *s, const char *key, char *val);
int parse_quoted_string(const char *string, char quote, char *val, int len);


#endif
