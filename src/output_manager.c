#include <stdio.h>
#include <fcntl.h>
#include <sys/select.h>
#include <sys/ioctl.h>
#include <time.h>
#include <unistd.h>
#include <string.h>

#include "output_manager.h"
#include "loader.h"

int gnuplot_config()
{
  int i;
  static int first_exec = 1;
  char varname[NAMELENGTH];
  /* color palette */
  if ( strncmp("acid",get_str("palette"),3) == 0 )
  {
    for (i = 0; i < 8; i++)
    {
      fprintf(GPLOTP,"set linetype %d  lc rgb '%s' \n", i+1, PALETTE_ACID[i]); 
    }
  }
  else if ( strncmp("qual",get_str("palette"),4) == 0 )
  {
    for (i = 0; i < 8; i++)
    {
      fprintf(GPLOTP,"set linetype %d  lc rgb '%s' \n", i+1, PALETTE_QUAL[i]); 
    }
  }
  else if ( strncmp("mono",get_str("palette"),4) == 0 )
  {
    for (i = 0; i < 8; i++)
    {
      fprintf(GPLOTP,"set linetype %d  lc rgb '%s' \n", i+1, PALETTE_MONO[0]); 
    }
  }
  else /* default: APPLE */
  {
    for (i = 0; i < 8; i++)
    {
      fprintf(GPLOTP,"set linetype %d  lc rgb '%s' \n", i+1, PALETTE_APPLE[i]); 
    }
  }

  fprintf(GPLOTP,"set term %s title \"odexp | %s\" font \"%s,%d\"\n", \
      get_str("terminal"), get_str("wintitle"), get_str("font"), get_int("fontsize"));
  fprintf(GPLOTP,"set border 1+2+16 lw 0.5 lc rgb \"grey20\"\n");
  fprintf(GPLOTP,"set border 1+2+16 lw 0.5 lc rgb \"grey20\"\n");
  fprintf(GPLOTP,"set xtics textcolor rgb \"grey20\"\n");
  fprintf(GPLOTP,"set ytics textcolor rgb \"grey20\"\n");
  fprintf(GPLOTP,"set ztics textcolor rgb \"grey20\"\n");
  fprintf(GPLOTP,"set tics nomirror out font \"%s,%d\"\n", \
      get_str("font"), get_int("fontsize"));
  fprintf(GPLOTP,"set mxtics\n");
  fprintf(GPLOTP,"set mytics\n");
  fprintf(GPLOTP,"set mztics\n");

  /* background */
  if ( strncmp("fancy",get_str("background"),5) == 0 )
  {
    fprintf(GPLOTP,"set object 99 circle at screen 0.75, -0.75 size screen 1 fillcolor rgb \""\
                    TERM_BG_COLOR2 "\" fillstyle solid behind\n");
    fprintf(GPLOTP,"set object 98 rectangle from screen 0,0 to screen 1,1 fillcolor rgb " "\""\
                    TERM_BG_COLOR1 "\" behind\n");
  }
  else if ( strncmp("none",get_str("background"),5) == 0 )
  {
    fprintf(GPLOTP,"unset object 98\nunset object 99\n"); 
  }
  else 
  {
    fprintf(GPLOTP,"unset object 98\nunset object 99\n"); 
  }

  fprintf(GPLOTP,"set grid xtics ytics ztics\n");
  if ( get_int("togglekey") )
  {
    fprintf(GPLOTP,"set key nobox noopaque\n");
  }
  else
  {
    fprintf(GPLOTP,"unset key\n");
  }

  /* run only at first execution */
  /* define gnuplot variables corresponding to plottable variables */
  if ( first_exec )
  {
    fprintf(GPLOTP,"%s = \"$1\"\n", get_str("indvar"));
    for (i = 0; i < SIM->nbr_var; i++)
    {
      translate(varname,SIM->varnames[i],"[]","_ ",NAMELENGTH);
      fprintf(GPLOTP,"%s = \"$%d\"\n", varname,i+2);
    }
    for (i = 0; i < SIM->nbr_aux; i++)
    {
      translate(varname,SIM->auxnames[i],"[]","_ ",NAMELENGTH);
      fprintf(GPLOTP,"%s = \"$%d\"\n", varname,SIM->nbr_var+i+2);
    }
    for (i = 0; i < SIM->nbr_psi; i++)
    {
      translate(varname,SIM->psinames[i],"[]","_ ",NAMELENGTH);
      fprintf(GPLOTP,"%s = \"$%d\"\n", varname,SIM->nbr_var+SIM->nbr_aux+i+2);
    }
    for (i = 0; i < SIM->nbr_expr; i++)
    {
      translate(varname,SIM->exprnames[i],"[]","_ ",NAMELENGTH);
      fprintf(GPLOTP,"%s = \"$%d\"\n", varname,SIM->nbr_var+SIM->nbr_aux+SIM->nbr_psi+i+2);
    }
    printf("\n  assigning variables to gnuplot, type 'gshow variable' to see them\n");
    first_exec = 0;
  }


  return 0;
}

int save_snapshot(nve init, nve mu, double_array tspan, const char *odexp_filename)
{
  int i;
  int opn;
  size_t linecap;
  char *line = NULL;
  size_t bgn = 0;
  FILE *fr;
  FILE *eqfr;
  char rootname[MAXROOTLENGTH];
  int  rootnamescanned = 0;
  char pop_buffer[MAXFILENAMELENGTH];
  char print_buffer[MAXFILENAMELENGTH];
  int len = *mu.max_name_length;
  clock_t time_stamp;

  par *pars = SIM->pop->start;

  if (*init.max_name_length > len)
  {
    len = *init.max_name_length;
  }

  time_stamp = clock(); /* get a time stamp */
  rootnamescanned = sscanf(rawcmdline,"%*[q*] %[a-zA-Z0-9_-]",rootname); /* get the first word after command s or q */


  if (rootnamescanned > 0)
  {
    snprintf(pop_buffer,MAXFILENAMELENGTH, ODEXPDIR "%s.%ju.pop",rootname,(uintmax_t)time_stamp);
    snprintf(print_buffer,MAXFILENAMELENGTH, ODEXPDIR "%s.%ju.eps",rootname,(uintmax_t)time_stamp);
  }  
  else
  {  
    snprintf(pop_buffer,MAXFILENAMELENGTH, ODEXPDIR "%s.%ju.pop", odexp_filename, (uintmax_t)time_stamp);
    snprintf(print_buffer,MAXFILENAMELENGTH, ODEXPDIR "%s.%ju.eps", odexp_filename, (uintmax_t)time_stamp);
  }

  /* open buffer parameter file (par) */
  

  if ( ( fr = fopen(pop_buffer,"w") ) == NULL )
  {
    PRINTERR("  File %s could not be opened. Nothing was written",pop_buffer);
  }
  else
  {
    fprintf(fr,"#%s\n",rawcmdline+1);
    fprintf(fr,"\n# --------------------------------------------------\n");
    fprintf(fr,"# Parameter file for the odexp system: '%s'\n", odexp_filename); 
    fprintf(fr,"\n# To load the parameter file from odexp, use the following command:\n");
    fprintf(fr,"# odexp> o %s\n", pop_buffer);
    fprintf(fr,"\n# To run %s using parameters in %s,\n# use the following command from the prompt:\n",odexp_filename,pop_buffer);
    fprintf(fr,"# prompt$ odexp -p %s %s\n",pop_buffer,odexp_filename);
    fprintf(fr,"#\n# associated figure file: %s of type %s\n",print_buffer,get_str("printsettings"));
    fprintf(fr,"# --------------------------------------------------\n");

    fprintf(fr,"\n# parameters/values\n");
    for(i=0;i<mu.nbr_el;i++)
    {
      fprintf(fr,"par %-*s %g {%s} # %s\n",\
          len,mu.name[i],mu.value[i],mu.attribute[i],mu.comment[i]);
    }

    fprintf(fr,"\n# initial conditions\n");
    for(i=0;i<init.nbr_el;i++)
    {
      if ( get_int("init") ) 
      {
        fprintf(fr,"init %-*s %g {%s} # initial expression: %s; initial comment: %s\n",\
            len,init.name[i],init.value[i],init.attribute[i],init.expression[i],init.comment[i]);
      }
      else
      {
        fprintf(fr,"init %-*s %s {%s} # initial comment: %s\n",\
            len,init.name[i],init.expression[i],init.attribute[i],init.comment[i]);
      }
    }    

    fprintf(fr,"\n# time span\ntimespan ");
    for(i=0;i<tspan.length;i++)
    {
      fprintf(fr,"%g ",tspan.array[i]);
    }
    fprintf(fr,"\n\n");

    fprintf(fr,"# options\n");
    for(i=0;i<NBROPTS;i++)
    {
      switch (GOPTS[i].valtype)
      {
        case 'd' :
          fprintf(fr,"opt %-*s %g\n",len,GOPTS[i].name,GOPTS[i].numval);
          break;
        case 'i' :
          fprintf(fr,"opt %-*s %d\n",len,GOPTS[i].name,GOPTS[i].intval);
          break;
        case 's' :
          fprintf(fr,"opt %-*s %s\n",len,GOPTS[i].name,GOPTS[i].strval);
      }
    }

    fprintf(fr,"\n# --------------------------------------------------\n");
    fprintf(fr,"# original equations, auxiliary variables and parametric expressions\n\n");

    
    if ( ( eqfr = fopen(odexp_filename,"r") ) == NULL )
    {
      PRINTERR("  File '%s' could not be opened. Not writing equations",odexp_filename);
      fclose(fr);
      return 1;
    }
    else
    {
      while( getline(&line,&linecap,eqfr) > 0)
      {
        bgn = strspn(line," \t"); /* ignore whitespaces */
        if ( strncasecmp(line + bgn,"par",3) == 0 ||
             strncasecmp(line + bgn,"times",5) == 0 ||
             strncasecmp(line + bgn,"init",4)  == 0 ||
             strncasecmp(line + bgn,"opt",3) == 0 ) 
        {
          fprintf(fr,"#    %s", line);
        }
        else 
        {
          opn = 0;
          sscanf(line + bgn,"d%*[^/]/dt %*[^{] %n{ ", &opn); /* get position of opening { bracket */
          fprintf(fr,"%.*s", (int)bgn + opn, line);
          if ( opn > 0 )
            {
              fprintf(fr,"# ");                                  /* comment out the attributes */
            }
          fprintf(fr,"%s", line + bgn + opn);
        }
        
      }
    }
    fclose(eqfr);

    /* particle alive at the end of the simulation */
    fprintf(fr,"\n# particles alive at the end of the simulation\n");
    while ( pars != NULL )
    {
      fprintf(fr,"# %d\n",pars->id); 
      pars = pars->nextel;
    }

    fclose(fr);

    printf("  wrote %s\n",pop_buffer);

  }


   /* try saving plot with name given in svalue */
  fprintf(GPLOTP,"set term push\n"); /* save term settings */
  fprintf(GPLOTP,"unset term\n"); /* unset all term-specific settings */
  fprintf(GPLOTP,"set term %s font \"%s,%d\"\n", \
      get_str("printsettings"), get_str("font"), get_int("fontsize"));
  fprintf(GPLOTP,"set tics nomirror out font \"%s,%d\"\n", \
      get_str("font"), get_int("fontsize"));
  fprintf(GPLOTP,"set output \"%s\"\n",print_buffer);
  fprintf(GPLOTP,"replot\n");
  fprintf(GPLOTP,"set term pop\n");
  fprintf(GPLOTP,"set tics nomirror out font \"%s,%d\"\n", \
      get_str("font"), get_int("fontsize"));
  fflush(GPLOTP);
  printf("  wrote %s\n",print_buffer);

  free(line);

  return 0;
}

int read_msg( void )
{
  fd_set fdset; /* file descriptor set */
  int rd;
  char gpchar;
  struct timeval tv;

  FD_ZERO(&fdset); /* clear FD SET */
  FD_SET(GPIN, &fdset);  /* include GPIN in FD SET */

  tv.tv_sec = 0;
  tv.tv_usec = GP_WAIT; /* microsec, default 0.02 sec */

  rd = select(GPIN + 1, &fdset, NULL, NULL, &tv);
  if ( rd < 1 )
  {
    return rd;
  }
  printf("%s",T_GPL);
  while ( read(GPIN, &gpchar, 1) > 0 ) /* read return 0 on EOF and -1 (err) 
                                          if file was marked for non-blocking I/O, 
                                          and no data were ready to be read */
  {
    putchar(gpchar);
  }
  printf("%s\n",T_NOR); 
  return rd;
}

int printf_msg_buff()
{
  int i;
  for ( i = 0; i < MSG_BUFF.nbr_msg; ++i)
  {
    printf("  %d: %s\n", i, MSG_BUFF.msg[ (MSG_BUFF.line - i -1 + SIZE_MSG_BUFFER) % 50 ]);
  }
  return MSG_BUFF.nbr_msg;
}



int printf_status_bar( double_array *tspan) 
{
  struct winsize w; /* get terminal window size */
  char status_str[86]; /* 79 chars + 6 to hold three wide-chars */
  int k = 0;
  ioctl(STDOUT_FILENO, TIOCGWINSZ, &w); /* get terminal window size in w */
  if ( w.ws_col > 79 )  
  {
    printf("\n"); /* down one line */
    printf("%s",T_BAR);
    while(w.ws_col - k++)
    {
      putchar(' ');  /* fill status bar with background T_BAR */ 
    }
    printf("%s","\033[G");  /* cursor back to col 1  */
    if ( get_int("hold") )
    {
      snprintf(status_str,3,"H ");
    }
    else if ( get_int("curves") == 1 )
    {
      snprintf(status_str,3,"U ");
    }
    else if ( get_int("curves") == 2 )
    {
      snprintf(status_str,3,"u ");
    }
    else
    {
      snprintf(status_str,3,"  ");
    }
    k = strlen(status_str);
    snprintf(status_str+k,80-k,"%s=%g  t=%g…%g ", get_str("actpar"), SIM->mu[get_int("actpar")], tspan->array[0], tspan->array[tspan->length-1]);
    k = strlen(status_str);
    snprintf(status_str+k,80-k,"%.1s ", get_str("popmode"));
    k = strlen(status_str);
    snprintf(status_str+k,80-k,"∫%s ", get_str("solver"));
    k = strlen(status_str);
    snprintf(status_str+k,80-k,"•%d ", get_int("res"));
    k = strlen(status_str);
    snprintf(status_str+k,80-k,"|%g| ", get_dou("abstol"));
    k = strlen(status_str);
    snprintf(status_str+k,80-k,"%%%g ", get_dou("reltol"));
    k = strlen(status_str);
    snprintf(status_str+k,80-k,"(%d)/%d/%d",  get_int("particle"),POP_SIZE,SIM->max_id);
    /* k = strlen(status_str) - 6; */ /* -6 to account for the three wide chars …, ∫ and • */
    printf("%.79s",status_str);
    printf("%s",T_NOR);
    printf("%s","\033[F");  /* up one line  */
  }
  return 0;
}

int printf_options(const char *optiontype)
{
  int i; 
  char last_option_type[NAMELENGTH]; 
  last_option_type[0] = 0; /* snprintf(last_option_type,NAMELENGTH,"");  */
  for(i=0;i<NBROPTS;i++)
  {
    if ( optiontype == NULL || strcmp(GOPTS[i].optiontype,optiontype) == 0 )
    {
      if ( strcmp(GOPTS[i].optiontype,last_option_type) )
      {
        printf("\n%-*s " HLINE "\n",NAME_COL_WIDTH,GOPTS[i].optiontype);
      }
      printf_option_line(i);
    }
    snprintf(last_option_type,NAMELENGTH,"%s",GOPTS[i].optiontype); 
  }
  return 1;
}

void printf_list_val(char type, int print_index, int nve_index, int padding, const nve *var, char *descr)
{
  printf("  %c[" T_IND "%d" T_NOR "]%-*s %s%*s " T_VAL "%10g " T_OPT "%-16s %-8s " T_DET "%s" T_NOR "\n",\
      type,print_index, padding, "",\
      var->name[nve_index],*var->max_name_length - mbstrlen(var->name[nve_index]),"",\
      var->value[nve_index],\
      descr,var->attribute[nve_index],var->comment[nve_index]);
#if 0
  printf("  %c[%s%d%s]%-*s %-*s = %s%10g%s %s%-16s %-8s %s%s%s\n",\
      type,T_IND,print_index,T_NOR, padding, "",\
      *var->max_name_length,var->name[nve_index],\
      T_VAL,var->value[nve_index],T_NOR,\
      T_OPT,descr,var->attribute[nve_index],T_DET,var->comment[nve_index],\
      T_NOR);
#endif
}

void printf_list_str(char type, int print_index, int nve_index, int padding, const nve *var)
{
  printf("  %c[" T_IND "%d" T_NOR "]%-*s %s%*s " T_EXPR "%s" T_OPT "%s " T_DET "%s" T_NOR "\n",\
      type,print_index, padding, "", var->name[nve_index], NAME_COL_WIDTH - mbstrlen(var->name[nve_index]),"",\
      var->expression[nve_index],\
      var->attribute[nve_index],\
      var->comment[nve_index]);
#if 0
  printf("  %c[%s%d%s]%-*s %-*s %s%s%s %s%s %s%s%s\n",\
      type,T_IND,print_index,T_NOR, padding, "", NAME_COL_WIDTH,var->name[nve_index],\
      T_EXPR,var->expression[nve_index],T_NOR,\
      T_OPT,var->attribute[nve_index],\
      T_DET,var->comment[nve_index],T_NOR);
#endif

}


void printf_SIM( void )
{
  par *p = SIM->pop->start;
  if ( p != NULL )
  {
    printf("particles:\n");
  }
  while ( p != NULL )
  {
    printf_particle(p);
    p = p->nextel;
  }
}

